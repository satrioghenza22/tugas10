public class CellphoneMain {
    public static void main(String[] args){
        Cellphone cp = new Cellphone("Nokia", "3310");
        cp.powerOn();
        cp.sleepmodeOn();
        cp.sleepmodeOff();
        cp.powerOff();

        // Dari sini semua function tidak bisa digunakan karena HP mati 
        cp.volumeDown();
        cp.sisaPulsa();

        // Setelah dihidupkan function bisa digunakan
        cp.powerOn();
        cp.volumeUp();
        cp.getVolume();
        cp.setVol(26);
        cp.getVolume();

        // Top Up pulsa diatas minimal 5000 & HP posisi hidup,
        // ketika posisi mati pulsa terkirim namun tidak bisa terlihat
        cp.topUpPulsa(15000);
        cp.sisaPulsa();

        // Menambahkan banyak contact dengan Array List
        Contact contact = new Contact("Noval", "137899");
        contact.tambahContact("Alfa", "13816");
        contact.tambahContact("Ishal", "13566");
        contact.tambahContact("Irfan", "13445");
        contact.tambahContact("Thie", "13643");
        contact.displayContacts();

        // Mencari Contact dengan Nama dan No
        contact.showContactByName("Irfan");
        contact.showContactByNoHP("13643");

    }
}
